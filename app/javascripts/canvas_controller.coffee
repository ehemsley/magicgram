module.exports = class CanvasController
  constructor: ->
    @canvas = document.createElement('canvas')
    @canvas.width = window.innerWidth
    @canvas.height = window.innerHeight
    @canvas.style.width = @canvas.width + 'px'
    @canvas.style.height = @canvas.height + 'px'

    @canvas.setAttribute("id", "canvasID")
    canvas_div = document.getElementById('canvas-container')
    canvas_div.appendChild(@canvas)

    @context = @canvas.getContext('2d')
    @context.textAlign = "center"
    @context.textBaseline = "center"

    @canvas.addEventListener('dragover', @HandleDragOver, false)
    @canvas.addEventListener('drop', @HandleFileSelect, false)

    @DrawIntroText()
    return

  DrawIntroText: =>
    @context.font = '50px Arial'
    @context.fillText('MagicGram', @canvas.width * 0.5, @canvas.height * 0.33)

    @context.font = '30px Arial'
    @context.fillText('Drag an image into the window to begin.', @canvas.width * 0.5, @canvas.height * 0.5)

    return

  GenerateGrayscaleMatrix: (canvas, width, height) ->
    grayscaleCanvas = document.createElement('canvas')
    grayscaleCanvas.width = width
    grayscaleCanvas.height = height

    grayscaleContext = grayscaleCanvas.getContext('2d')
    grayscaleContext.drawImage(@canvas, 0, 0)
    @ClearCanvas()

    grayscale_data = (0 for [0..(grayscaleCanvas.height-1)*(grayscaleCanvas.width-1)])
    grayscale_matrix = ((0 for [0..grayscaleCanvas.height-1]) for [0..grayscaleCanvas.width-1])

    imageData = grayscaleContext.getImageData(0, 0, grayscaleCanvas.width, grayscaleCanvas.height)
    data = imageData.data

    counter = 0
    for pixel, i in data by 4
      brightness = 0.34 * data[i] + 0.5 * data[i + 1] + 0.16 * data[i + 2]
      grayscale_data[counter] = Math.floor(brightness)
      counter += 1

    for x in [0..grayscaleCanvas.width-1]
      for y in [0..grayscaleCanvas.height-1]
        grayscale_matrix[x][y] = 255 - grayscale_data[x + ((grayscaleCanvas.width) * y)]

    return grayscale_matrix

  GenerateRandomDotCanvas: (grayscale_matrix, width, height) ->
    randomDotCanvas = document.createElement('canvas')
    randomDotCanvas.width = width
    randomDotCanvas.height = height

    randomDotContext = randomDotCanvas.getContext('2d')

    matrix = ((0 for [0..height-1]) for [0..width-1])

    for x in [0..width-1]
      for y in [0..height-1]
        if matrix[x][y] == 0
          randomDotContext.fillStyle = 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ')'

          x_pos = x
          while x_pos < width
            randomDotContext.fillRect(x_pos, y, 1, 1)
            matrix[x_pos][y] = 1

            displacement = 100 + Math.floor(grayscale_matrix[x_pos][y] * 0.125)
            x_pos = x_pos + displacement

    return randomDotCanvas

  DrawStereogram: (width, height) =>
    grayscale_matrix = @GenerateGrayscaleMatrix(@canvas, width, height)
    randomDotCanvas = @GenerateRandomDotCanvas(grayscale_matrix, width, height)
    @context.drawImage(randomDotCanvas, parseInt((@canvas.width - width) * 0.5), parseInt((@canvas.height - height) * 0.5))
    cancelAnimationFrame(@spinnerFrame)

    return

  ClearCanvas: =>
    @context.clearRect(0, 0, @canvas.width, @canvas.height)
    return

  HandleDragOver: (event) =>
    event.stopPropagation()
    event.preventDefault()

    event.dataTransfer.dropEffect = 'link'
    return

  HandleFileSelect: (event) =>
    event.stopPropagation()
    event.preventDefault()

    loaded = false
    file = event.dataTransfer.files[0]

    @ClearCanvas()
    @context.fillText('Processing image...', @canvas.width * 0.5, @canvas.height * 0.5)

    img = document.createElement('img')
    img.src = window.URL.createObjectURL(file)
    img.onload = =>
      @context.drawImage(img, 0,0)
      @DrawStereogram(img.width, img.height)
      window.URL.revokeObjectURL(this.src)

    return
